<?php

########################################################################
# Extension Manager/Repository config file for ext "xf_awstats".
#
# Auto generated 01-07-2012 00:49
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'AWStats',
	'description' => 'Includes the AWStats logfile analyzer as a backend module. This is a fork of ics_awstats to continue development.',
	'category' => 'module',
	'shy' => 0,
	'version' => '0.7.0',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => 'mod1',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Xaver Maierhofer',
	'author_email' => 'xaver.maierhofer@xwissen.info',
	'author_company' => 'Sam-City',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'php' => '5.3.0-0.0.0',
			'typo3' => '4.5.0-6.1.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

?>