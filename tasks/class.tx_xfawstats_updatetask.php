<?php

class tx_xfawstats_updatetask extends tx_scheduler_Task {

	public function execute() {

		$awstats = t3lib_div::makeInstance('tx_xfawstats_awstats');

		// do the job
		$logconfigs = $awstats->get_logconfigs();
		foreach ( $logconfigs as $lfile => $logconfig ) {
			if (! $logconfig['cron_update']) {
				continue;
			}
			$output = $awstats->call_awstats_cli_update($lfile);
			if (!is_numeric($output)) {
				foreach (explode("\n", $output) as $line) {
					if (strpos($line, 'Error') !== FALSE) {
						$GLOBALS['BE_USER']->simplelog($line, 'xf_awstats', 1);
					}
				}
			} else {
				switch ($output) {
					case tx_xfawstats_awstats::$ERR_LOGFILE_NOT_CONFIGURED:
						echo "Error: Logfile not configured\n";
						$GLOBALS['BE_USER']->simplelog('Error: Logfile not configured', 'xf_awstats', 1);
						return false;
					case tx_xfawstats_awstats::$ERR_AWSTATS_CALL_FAILED:
						$GLOBALS['BE_USER']->simplelog('Error: AWStats call failed', 'xf_awstats', 1);
						return false;
					case tx_xfawstats_awstats::$ERR_UPDATE_IS_LOCKED:
						$GLOBALS['BE_USER']->simplelog('Error: Update is locked', 'xf_awstats', 1);
						return false;
					default:
						$GLOBALS['BE_USER']->simplelog('Error: Unknown error', 'xf_awstats', 1);
						return false;
				}
			}
		}

		return true;

	} // End of Method: execute()

} // End of class: tx_xfawstats_UpdateTask

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/xf_awstats/tasks/class.tx_xfawstats_UpdateTask.php']) {
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/xf_awstats/task/class.tx_xfawstats_UpdateTask.php']);
}

?>